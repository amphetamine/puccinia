/*
 * 2.3. Replies from Tor to the controller
 *
 *     Reply = SyncReply / AsyncReply
 *     SyncReply = *(MidReplyLine / DataReplyLine) EndReplyLine
 *     AsyncReply = *(MidReplyLine / DataReplyLine) EndReplyLine
 *
 *     MidReplyLine = StatusCode "-" ReplyLine
 *     DataReplyLine = StatusCode "+" ReplyLine CmdData
 *     EndReplyLine = StatusCode SP ReplyLine
 *     ReplyLine = [ReplyText] CRLF
 *     ReplyText = XXXX
 *     StatusCode = 3DIGIT
 *
 *   Multiple lines in a single reply from Tor to the controller are guaranteed
 *   to share the same status code. Specific replies are mentioned below in
 *   section 3, and described more fully in section 4.
 *
 *   [Compatibility note:  versions of Tor before 0.2.0.3-alpha sometimes
 *   generate AsyncReplies of the form "*(MidReplyLine / DataReplyLine)".
 *   This is incorrect, but controllers that need to work with these
 *   versions of Tor should be prepared to get multi-line AsyncReplies with
 *   the final line (usually "650 OK") omitted.]
 *
 * So for example, "PROTOCOLINFO 1" returns something like this from Tor:
 *
 * 250-PROTOCOLINFO 1
 * 250-AUTH METHODS=NULL
 * 250-VERSION Tor="0.2.7.6"
 * 250 OK
 *
 */

/// The status codes returned by the Tor process.
#[derive(Debug)]
pub enum Status {
    /// Code 250.
    Ok(String),
    /// Code 251.
    UnnecessaryOperation(String),
    /// Code 451.
    ResourceExhausted(String),
    /// Code 500.
    ProtocolError(String),
    /// Code 510.
    UnrecognizedCommand(String),
    /// Code 511.
    UnimplementedCommand(String),
    /// Code 512.
    CommandError(String),
    /// Code 513.
    UnrecognizedArgument(String),
    /// Code 514.
    AuthenticationRequired(String),
    /// Code 515.
    BadAuthentication(String),
    /// Code 550.
    UnspecifiedError(String),
    /// Code 551.
    InternalError(String),
    /// Code 552.
    UnrecognizedEntity(String),
    /// Code 553.
    InvalidConfiguration(String),
    /// Code 554.
    InvalidDescriptor(String),
    /// Code 555.
    UnmanagedEntity(String),
    /// Code 650.
    ///
    /// This reply can contain information about a received signal or
    /// configuration change, such as `650 SIGNAL RELOAD`.
    AsyncEvent(String),
    /// Unrecognized message from Tor.
    Unknown(String)
}

/// The fields returned by the Tor process in response to a PROTOCOLINFO
/// message.
#[derive(Debug, PartialEq, Eq)]
pub struct ProtocolInfo {
    /// This is the only mandatory response for a PROTOCOLINFO response.
    pub protocol_version: String,
    pub tor_version: Option<String>,
    pub auth_methods: Option<Vec<String>>,
//    pub unknown_auth_methods: Option<Vec<String>>,
    pub cookie_path: Option<String>
}

/// The fields returned by the Tor process in response to an AUTHCHALLENGE
/// message.
#[derive(Debug, PartialEq, Eq)]
pub struct AuthChallenge {
    pub server_hash: String,
    pub server_nonce: String
}

/// Enum wrapped around the different possible reply structs.
#[derive(Debug, PartialEq, Eq)]
pub enum Reply {
    /// The PROTOCOLINFO reply, with a fancy struct to store fields.
    ProtocolInfo(ProtocolInfo),
    /// Reply when there's an error in authentication. (Also catches 551
    /// internal error, when that error is caused by bad auth.)
    AuthError(String),
    /// Reply to a well-formed AUTHCHALLENGE from the client.
    AuthChallenge(AuthChallenge),
    /// Catch-all reply.
    Unknown(Vec<String>)
}

/// Parses the status message (the first line) from the Tor process after a
/// message is sent.
pub fn parse_status(tor_msg: &String) -> Status {
    let status = &tor_msg[0..3];
    let status_msg = tor_msg[4..].to_string();
    match status {
        "250" => Status::Ok(status_msg),
        "251" => Status::UnnecessaryOperation(status_msg),
        "451" => Status::ResourceExhausted(status_msg),
        "500" => Status::ProtocolError(status_msg),
        "510" => Status::UnrecognizedCommand(status_msg),
        "511" => Status::UnimplementedCommand(status_msg),
        "512" => Status::CommandError(status_msg),
        "513" => Status::UnrecognizedArgument(status_msg),
        "514" => Status::AuthenticationRequired(status_msg),
        "515" => Status::BadAuthentication(status_msg),
        "550" => Status::UnspecifiedError(status_msg),
        "551" => Status::InternalError(status_msg),
        "552" => Status::UnrecognizedEntity(status_msg),
        "553" => Status::InvalidConfiguration(status_msg),
        "554" => Status::InvalidDescriptor(status_msg),
        "555" => Status::UnmanagedEntity(status_msg),
        "650" => Status::AsyncEvent(status_msg),
        _ => Status::Unknown(status_msg)
    }
}

/// Parses 
pub fn parse_reply(tor_msg: &Vec<String>) -> Reply {
    let status = parse_status(&tor_msg[0]);
    match status {
        Status::Ok(msg) => {
            if msg.contains("PROTOCOLINFO") {
                let proto_version = tor_msg.iter()
                // we have to use .find() since infolines may appear in any
                // order according to torspec
                    .find(|msg| msg.contains("PROTOCOLINFO"))
                    .and_then(|proto_str| Some(
                        proto_str
                            .trim_left_matches("250-PROTOCOLINFO")
                            .to_string()))
                // unwrap for now since it's a required response
                    .unwrap();
                let tor_version = tor_msg.iter()
                    .find(|msg| msg.contains("VERSION"))
                    .and_then(|ver_str| ver_str.split("\"").nth(1))
                    .and_then(|fmt_ver_str| Some(fmt_ver_str.to_string()));
                let auth_methods: Option<Vec<String>> = tor_msg.iter()
                    .find(|msg| msg.contains("AUTH"))
                    .and_then(|whole_auth_str| Some(
                        whole_auth_str.trim_left_matches("250-AUTH METHODS=")))
                    .and_then(|auths_str| Some(
                        auths_str
                            .split(",")
                        // rigmarole to drop COOKIEFILE line from result
                            .take_while(|&maybe_auth| !maybe_auth
                                        .contains("COOKIEFILE"))
                            .map(|method_str| method_str.to_string())
                            .collect::<Vec<String>>()));
                let cookie_path = tor_msg.iter()
                    .find(|msg| msg.contains("COOKIEFILE"))
                    .and_then(|cookie_str| Some(
                        cookie_str
                            .trim_left_matches("COOKIEFILE=")
                            .to_string()
                    ));

                Reply::ProtocolInfo(ProtocolInfo {
                    protocol_version: proto_version,
                    tor_version: tor_version,
                    auth_methods: auth_methods,
                    cookie_path: cookie_path
                })
            } else if msg.contains("AUTHCHALLENGE") {
                // abbreviated example
                // ["250 AUTHCHALLENGE SERVERHASH=6976C1CA9 SERVERNONCE=83D275A9C3E7F2"]
                let mut val_iter = msg
                    .split("=")
                    .map(|split_str| split_str.split(" ").nth(0).unwrap())
                    .skip(1);
                Reply::AuthChallenge(AuthChallenge {
                    server_hash: val_iter.next().unwrap().to_string(),
                    server_nonce: val_iter.next().unwrap().to_string()
                })
            } else {
                // TODO replace this with other reply types
                //
                // NOTE Ok replies don't actually report what they are in reply
                // to, so Unknown might show up often
                Reply::Unknown(tor_msg.to_owned())
            }
        },
        Status::BadAuthentication(msg) => {
            // NOTE This shouldn't happen within our API, but we handle it
            // anyway just in case
            Reply::AuthError(msg.to_owned())
        },
        _ => Reply::Unknown(tor_msg.to_owned())
    }
}
