use std::fmt;
use control::ControlSocket;

/* From the Tor spec:
 *
 * 2.2. Commands from controller to Tor
 *
 *     Command = Keyword OptArguments CRLF / "+" Keyword OptArguments CRLF
 *     CmdData
 *     Keyword = 1*ALPHA
 *     OptArguments = [ SP *(SP / VCHAR) ]
 *
 *   A command is either a single line containing a Keyword and arguments, or a
 *   multiline command whose initial keyword begins with +, and whose data
 *   section ends with a single "." on a line of its own.  (We use a special
 *   character to distinguish multiline commands so that Tor can correctly parse
 *   multi-line commands that it does not recognize.) Specific commands and
 *   their arguments are described below in section 3.
 *
 *
 * So for example that means either "COMMAND ARGS \r\n", or, a multiline command
 * like:
 *
 * + COMMAND ARGS
 * CMDDATA
 * CMDDATA
 * .
 *
 */

/// The possible reasons for closing a stream.
///
/// See the [tor
/// spec](https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt) for more
/// info.
pub enum StreamCloseReason {
    /// A catch-all reason.
    MISC,
    /// Couldn't look up hostname.
    RESOLVEFAILED,
    /// Remote host refused connection.
    CONNECTREFUSED,
    /// OR refuses to connect to host or port.
    EXITPOLICY,
    /// CIRCUIT is being destroyed.
    DESTROY,
    /// Anonymized TCP connection was closed.
    DONE,
    /// Connection timed out, or OR timed out while connecting.
    TIMEOUT,
    /// Routing error while attempting to contact destination.
    NOROUTE,
    /// OR is temporarily hibernating.
    HIBERNATING,
    /// Internal error at the OR.
    INTERNAL,
    /// OR has no resources to fulfill request.
    RESOURCELIMIT,
    /// Connection was unexpectedly reset.
    CONNRESET,
    /// Closing connection because of Tor protocol violations.
    TORPROTOCOL,
    /// Client sent RELAY_BEGIN_DIR to a non-directory relay.
    NOTDIRECTORY
}

#[derive(Debug)]
/// The signal types the Tor process will accept.
pub enum Signal {
    /// Reload config items. (Same as HUP.)
    RELOAD,
    /// Controlled shutdown of the Tor process. (Same as INT.)
    ///
    /// If there are listeners, they will be closed and exit will occur after
    /// ShutdownWaitLength seconds (config).
    SHUTDOWN,
    /// Dump process stats. (Same as USR1.)
    DUMP,
    /// Switch all open logs to log level debug. (Same as USR2.)
    DEBUG,
    /// Immediate shutdown. (Same as TERM.)
    HALT,
    /// Same as RELOAD.
    HUP,
    /// Same as SHUTDOWN.
    INT,
    /// Same as DUMP.
    USR1,
    /// Same as DEBUG.
    USR2,
    /// Same as HALT.
    TERM,
    /// Switch to clean circuits. New application requests will not share any
    /// with old ones.
    ///
    /// Client-side DNS cache will be cleared.
    NEWNYM,
    /// Clears client-side DNS cache for all hostnames.
    CLEARDNSCACHE,
    /// Causes Tor process to dump an unscheduled heartbeat message.
    HEARTBEAT
}

// https://stackoverflow.com/questions/32710187/get-enum-as-string/32712140#32712140
/// Allows the enum variants to be converted directly to their string
/// representation.
impl fmt::Display for Signal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

#[derive(Debug)]
/// The key types ADD_ONION accepts.
pub enum OnionKeyType {
    /// The server should generate a key with the specified algorithm.
    NEW,
    /// The server should use the 1024-bit RSA key provided.
    RSA1024
}

/// Allows the enum variants to be converted directly to their string
/// representation.
impl fmt::Display for OnionKeyType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
/// The key blob parameter used in ADD_ONION.
pub enum OnionKeyBlob<'a> {
    /// The server should generate a key using the "best" supported
    /// algorithm. Used if KeyType is "NEW".
    BEST,
    /// The server should generate a 1024-bit RSA key. Used if KeyType is "NEW".
    RSA1024,
    /// A serialized private key without whitespace.
    String(&'a str)
}

/// Allows the enum variants to be converted directly to their string
/// representation.
impl<'a> fmt::Display for OnionKeyBlob<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
/// Purpose types for circuit creation.
pub enum Purpose {
    Bridge,
    Controller,
    General,
}

/// Allows the enum variants to be converted directly to their string
/// representation.
impl<'a> fmt::Display for Purpose {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

/// A trait for abstracting the complexity of sending and receiving control
/// messages. All of the functions take params as specified in the [Tor control
/// spec][spec] and produce compliant strings.
///
/// [spec]: https://gitweb.torproject.org/torspec.git/tree/control-spec.txt 
pub trait ControlProtocol {
    /// Change the value of one or more configuration variables.
    fn setconf(vars: Vec<(&str, Option<&str>)>) -> String;
    /// Remove all settings for given configuration options entirely, assign
    /// their default value (if any), and then assign the String provided.
    fn resetconf(vars: Vec<(&str, Option<&str>)>) -> String;
    /// Request the value of a configuration variable.
    fn getconf(keys: Vec<&str>) -> String;
    /// Request the server to inform the client about interesting events.
    fn setevents(events: Vec<&str>, extended: bool) -> String;
    /// Authenticate with the Tor process.
    fn authenticate(pass: &str) -> String;
    /// Save the current Tor config options to its torrc.
    fn saveconf() -> String;
    /// Sends a signal to the Tor process.
    ///
    /// Valid signals are:
    ///
    /// - "RELOAD"
    /// - "SHUTDOWN"
    /// - "DUMP"
    /// - "DEBUG"
    /// - "HALT"
    /// - "HUP"
    /// - "INT"
    /// - "USR1"
    /// - "USR2"
    /// - "TERM"
    /// - "NEWNYM"
    /// - "CLEARDNSCACHE"
    /// - "HEARTBEAT"
    fn signal(signal: Signal) -> String;
    /// Tells Tor process to redirect SOCKS requests from one address to
    /// another.
    ///
    /// Inputs are in the form `(source_address, dest_address)`.
    fn mapaddress(addresses: Vec<(&str, &str)>) -> String;
    /// Gets information from the Tor process not stored in the config file.
    fn getinfo(keys: Vec<&str>) -> String;
    /// Tells the Tor process either to build a new circuit or extend an
    /// existing one.
    ///
    /// If the `circuit_id` is 0, the Tor process will build a new circuit. In
    /// this case, you can also provide a path in the form of a
    /// ServerSpec. Also, you can specify a purpose in this case.
    fn extendcircuit(circuit_id: &str,
                     server_spec: Option<Vec<&str>>,
                     purpose: Option<Purpose>) -> String;
    /// Sets an existing circuit's purpose.
    fn setcircuitpurpose(circuit_id: &str, purpose: Purpose) -> String;
    /// Attaches a stream to a specified circuit. (Circuits have many streams,
    /// but each stream only has one circuit.)
    fn attachstream(stream_id: &str,
                    circuit_id: &str,
                    hop_num: Option<usize>) -> String;
    /// Tells the Tor process about a new descriptor.
    fn postdescriptor(desc: &str,
                      purpose: Option<Purpose>,
                      cache: Option<bool>) -> String;
    /// Changes the exit address of a specified stream.
    fn redirectstream(stream_id: &str,
                      address: &str,
                      port: Option<usize>) -> String;
    /// Closes the specified stream.
    fn closestream(stream_id: &str,
                   reason: StreamCloseReason,
                   flags: Option<Vec<&str>>) -> String;
    /// Closes the specified circuit.
    fn closecircuit(circuit_id: &str, flags: Option<Vec<&str>>) -> String;
    /// Tells the Tor process to hang up on this controller socket.
    fn quit() -> String;
    /// Tells the Tor process to enable additional features on the protocol.
    fn usefeature(features: Vec<&str>) -> String;
    /// Starts a remote hostname lookup request for every specified request. If
    /// `mode=reverse` is specified as one of the options, the lookup will be
    /// a reverse lookup.
    ///
    /// Note that while none of the arguments are required, calling resolve
    /// without them is a no-op(?)
    fn resolve(options: Option<Vec<&str>>,
               addresses: Option<Vec<&str>>) -> String;
    /// Requests the Tor process to report basic info about its control
    /// protocol, including auth method.
    fn protocolinfo() -> String;
    /// Tells the Tor process to load the input config text as its new config.
    fn loadconf(conf_text: &str) -> String;
    /// Tells the Tor process that it should shut down when this control
    /// connection is closed.
    ///
    /// Note that multiple sockets can send this command, and Tor will obey all
    /// of them at the same time.
    fn takeownership() -> String;
    /// Authenticates with a Tor process that responds to the "SAFECOOKIE"
    /// method.
    fn authchallenge(safe_cookie: &str, client_nonce: &str) -> String;
    /// Drops all of the guard nodes for this process.
    fn dropguards() -> String;
    /// Launches hidden service descriptor fetches for a given HSAddress or
    /// DescId.
    ///
    /// A DescId can also be passed in instead of a hidden service address, but
    /// it must be formatted properly. According to the spec, this is:
    ///
    /// ```text
    /// "v" Version "-" DescId
    /// ```
    ///
    /// where Version is 2 and DescId is 32 base32 chars.
    fn hsfetch(hs_address: &str, server_name: Option<&str>) -> String;
    /// Creates a new hidden service on the Tor process, with the specified
    /// private key and algorithm.
    fn add_onion(key_type: OnionKeyType,
                 key_blob: OnionKeyBlob,
                 flags: Option<Vec<&str>>,
                 port: Option<(usize, Option<&str>)>,
                 client_auths: Option<Vec<(&str, Option<&str>)>>) -> String;
    /// Deletes a hidden service from the Tor process.
    ///
    /// Note that this does not terminate existing connections to that service.
    fn del_onion(service_id: &str) -> String;
    /// Launches a hidden service descriptor upload to the specified hidden
    /// service descriptors.
    fn hspost(descriptor: &str, server_id: Option<&str>) -> String;
}

impl<'a, T> ControlProtocol for T where T: ControlSocket<'a> {    
    fn setconf(vars: Vec<(&str, Option<&str>)>) -> String {
        // NOTE not that it matters, but pushing onto a well-allocated string is
        // I guess the fastest way to build strings in rust:
        // https://github.com/hoodie/concatenation_benchmarks-rs
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("SETCONF");

        for (key, maybe_val) in vars {
            cmd_string.push_str(" ");
            cmd_string.push_str(key);
            if let Some(val) = maybe_val {
                cmd_string.push_str("=");
                cmd_string.push_str(val);
            }
        }

        cmd_string.push_str("\r\n");

        cmd_string
    }

    fn resetconf(vars: Vec<(&str, Option<&str>)>) -> String {
        // NOTE not that it matters, but pushing onto a well-allocated string is
        // I guess the fastest way to build strings in rust:
        // https://github.com/hoodie/concatenation_benchmarks-rs
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("RESETCONF");

        for (key, maybe_val) in vars {
            cmd_string.push_str(" ");
            cmd_string.push_str(key);
            if let Some(val) = maybe_val {
                cmd_string.push_str("=");
                cmd_string.push_str(val);
            }
        }

        cmd_string.push_str("\r\n");

        cmd_string
    }

    fn getconf(keys: Vec<&str>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("GETCONF");

        for key in keys {
            cmd_string.push_str(" ");
            cmd_string.push_str(key);
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn setevents(events: Vec<&str>, extended: bool) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("SETEVENTS");

        if extended {
            cmd_string.push_str(" EXTENDED");
        }

        for event in events {
            cmd_string.push_str(" ");
            cmd_string.push_str(event);
        }

        cmd_string.push_str("\r\n");

        cmd_string
    }

    fn authenticate(pass: &str) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("AUTHENTICATE ");
        cmd_string.push_str(pass);
        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn saveconf() -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("SAVECONF");
        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn mapaddress(addresses: Vec<(&str, &str)>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("MAPADDRESS");

        for (source, sink) in addresses {
            cmd_string.push(' ');
            cmd_string.push_str(source);
            cmd_string.push('=');
            cmd_string.push_str(sink);
        }
        
        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn signal(signal: Signal) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("SIGNAL ");
        cmd_string.push_str(signal.to_string().as_str());
        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn getinfo(keys: Vec<&str>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("GETINFO");

        for key in keys {
            cmd_string.push(' ');
            cmd_string.push_str(key);
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn extendcircuit(circuit_id: &str,
                     server_specs: Option<Vec<&str>>,
                     purpose: Option<Purpose>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("EXTENDCIRCUIT ");
        cmd_string.push_str(circuit_id);

        if let Some(specs) = server_specs {
            cmd_string.push(' ');
            for spec in specs {
                cmd_string.push_str(spec);
                cmd_string.push(',');
            }

            cmd_string.pop(); // remove last comma
        }

        if let Some(purp) = purpose {
            cmd_string.push_str(" purpose=");
            cmd_string.push_str(purp.to_string().to_lowercase().as_str());
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn setcircuitpurpose(circuit_id: &str, purpose: Purpose) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("SETCIRCUITPURPOSE ");
        cmd_string.push_str(circuit_id);

        cmd_string.push_str(" purpose=");
        cmd_string.push_str(purpose.to_string().to_lowercase().as_str());

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn attachstream(stream_id: &str,
                    circuit_id: &str,
                    hop_num: Option<usize>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("ATTACHSTREAM ");
        cmd_string.push_str(stream_id);
        cmd_string.push(' ');
        cmd_string.push_str(circuit_id);

        if let Some(hpn) = hop_num {
            cmd_string.push_str(" HOP=");
            cmd_string.push_str(hpn.to_string().as_str());
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn postdescriptor(desc: &str,
                      purpose: Option<Purpose>,
                      cache: Option<bool>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("+POSTDESCRIPTOR");

        if let Some(purp) = purpose {
            cmd_string.push_str(" purpose=");
            cmd_string.push_str(purp.to_string().to_lowercase().as_str());
        }
        if let Some(cach) = cache {
            cmd_string.push_str(" cache=");
            match cach {
                true => cmd_string.push_str("yes"),
                false => cmd_string.push_str("no")
            }
        }
        cmd_string.push_str("\r\n");

        cmd_string.push_str(desc);
        cmd_string.push_str("\r\n.\r\n");

        cmd_string
    }

    fn redirectstream(stream_id: &str,
                      address: &str,
                      port: Option<usize>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("REDIRECTSTREAM ");
        cmd_string.push_str(stream_id);
        cmd_string.push(' ');
        cmd_string.push_str(address);

        if let Some(prt) = port {
            cmd_string.push(' ');
            cmd_string.push_str(prt.to_string().as_str());
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn closestream(stream_id: &str,
                   reason: StreamCloseReason,
                   flags: Option<Vec<&str>>) -> String {
        let mut cmd_string = String::with_capacity(1024);
        
        cmd_string.push_str("CLOSESTREAM ");
        cmd_string.push_str(stream_id);
        cmd_string.push(' ');
        // NOTE we increment the enum numeric id because the Tor spec starts
        // them with one, not zero
        cmd_string.push_str(((reason as usize) + 1).to_string().as_str());

        if let Some(flgs) = flags {
            for flag in flgs {
                cmd_string.push(' ');
                cmd_string.push_str(flag);
            }
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn closecircuit(circuit_id: &str, flags: Option<Vec<&str>>) -> String {
        let mut cmd_string = String::with_capacity(1024);
        cmd_string.push_str("CLOSECIRCUIT ");
        cmd_string.push_str(circuit_id);

        if let Some(flgs) = flags {
            for flag in flgs {
                cmd_string.push(' ');
                cmd_string.push_str(flag);
            }
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn quit() -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("QUIT");
        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn usefeature(features: Vec<&str>) -> String {
        let mut cmd_string = String::with_capacity(1024);
        cmd_string.push_str("USEFEATURE");

        for feat in features {
            cmd_string.push(' ');
            cmd_string.push_str(feat);
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn resolve(options: Option<Vec<&str>>,
               addresses: Option<Vec<&str>>) -> String {
        let mut cmd_string = String::with_capacity(1024);
        cmd_string.push_str("RESOLVE");

        if let Some(opts) = options {
            for opt in opts {
                cmd_string.push(' ');
                cmd_string.push_str(opt);
            }
        }
        if let Some(addrs) = addresses {
            for addr in addrs {
                cmd_string.push(' ');
                cmd_string.push_str(addr);
            }
        }

        cmd_string.push_str("\r\n");
        cmd_string
    }

    fn protocolinfo() -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("PROTOCOLINFO 1");
        cmd_string.push_str("\r\n");
        cmd_string
    }
    
    fn loadconf(conf_text: &str) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("+LOADCONF");
        cmd_string.push_str("\r\n");
        cmd_string.push_str(conf_text);
        cmd_string.push_str("\r\n");
        cmd_string.push('.');
        cmd_string.push_str("\r\n");
        cmd_string
    }
    
    fn takeownership() -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("TAKEOWNERSHIP");
        cmd_string.push_str("\r\n");
        cmd_string
    }
    
    fn authchallenge(safe_cookie: &str, client_nonce: &str) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("AUTHCHALLENGE ");
        cmd_string.push_str(safe_cookie);
        cmd_string.push(' ');
        cmd_string.push_str(client_nonce);
        cmd_string.push_str("\r\n");
        
        cmd_string
    }
    
    fn dropguards() -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("DROPGUARDS");
        cmd_string.push_str("\r\n");
        cmd_string
    }
    
    fn hsfetch(hs_address: &str, server_name: Option<&str>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("HSFETCH ");
        cmd_string.push_str(hs_address);

        if let Some(srv_name) = server_name {
            cmd_string.push_str(" SERVER=");
            cmd_string.push_str(srv_name);
        }
        
        cmd_string.push_str("\r\n");
        cmd_string
    }
    
    fn add_onion(key_type: OnionKeyType,
                 key_blob: OnionKeyBlob,
                 flags: Option<Vec<&str>>,
                 port: Option<(usize, Option<&str>)>,
                 client_auths: Option<Vec<(&str, Option<&str>)>>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("ADD_ONION ");
        cmd_string.push_str(key_type.to_string().as_str());
        cmd_string.push(':');

        // temp var for reasons of lifetime
        let blob = match key_blob {
            // pluck the key out of the enum if that was what was passed in,
            // otherwise turn the enum into a string
            //
            // NOTE these are actually two different "to_string"
            // functions. weird huh?
            OnionKeyBlob::String(key) => key.to_string(),
            _ => key_blob.to_string()
        };
        
        cmd_string.push_str(blob.as_str());

        if let Some(flgs) = flags {
            cmd_string.push_str(" Flags=");
            
            for flag in flgs {
                cmd_string.push_str(flag);
                cmd_string.push(',');
            }

            // pop off last comma
            cmd_string.pop();
        }

        if let Some(prt) = port {
            cmd_string.push_str(" Port=");
            cmd_string.push_str(prt.0.to_string().as_str());
            if let Some(target_port) = prt.1 {
                cmd_string.push(',');
                cmd_string.push_str(target_port);
            }
        }
            
        if let Some(cli_auths) = client_auths {
            for (cli_name, cli_blob) in cli_auths {
                cmd_string.push_str(" ClientAuth=");
                cmd_string.push_str(cli_name);

                if let Some(blob) = cli_blob {
                    cmd_string.push(':');
                    cmd_string.push_str(blob);
                }
            }
        }
        
        cmd_string.push_str("\r\n");
        cmd_string
    }
    
    fn del_onion(service_id: &str) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("DEL_ONION ");
        cmd_string.push_str(service_id);
        cmd_string.push_str("\r\n");
        
        cmd_string
    }
    
    fn hspost(descriptor: &str, server_id: Option<&str>) -> String {
        let mut cmd_string = String::with_capacity(1024);

        cmd_string.push_str("+HSPOST");
        if let Some(serv_id) = server_id {
            cmd_string.push_str(" SERVER=");
            cmd_string.push_str(serv_id);
        }
        
        cmd_string.push_str("\r\n");
        cmd_string.push_str(descriptor);
        cmd_string.push_str("\r\n.\r\n");
        
        cmd_string
    }
}

#[cfg(test)]
mod tests {
    use super::{ControlProtocol,
                OnionKeyType,
                OnionKeyBlob,
                Purpose,
                Signal,
                StreamCloseReason};
    use control::DomainSocket;

    // Each of the message tests follows the pattern "monadic, variadic", where
    // the monadic test just checks to see if one of each required argument
    // produces a valid message. The variadic test(s) checks to see if adding or
    // varying other arguments still produces something on spec.

    #[test]
    fn setconf_monadic() {
        let msg = DomainSocket::setconf(vec![("ORPort", Some("443"))]);
        assert_eq!(msg, "SETCONF ORPort=443\r\n");
    }
    
    #[test]
    fn setconf_variadic() {
        let msg = DomainSocket::setconf(
            vec![("ORPort", Some("443")),
                 ("ORListenAddress", None)]);
        assert_eq!(msg, "SETCONF ORPort=443 ORListenAddress\r\n");
    }
    
    #[test]
    fn resetconf_monadic() {
        let msg = DomainSocket::resetconf(vec![("ORPort", Some("443"))]);
        assert_eq!(msg, "RESETCONF ORPort=443\r\n");
    }
    
    #[test]
    fn resetconf_variadic() {
        let msg = DomainSocket::resetconf(
            vec![("ORPort", Some("443")),
                 ("ORListenAddress", None)]);
        assert_eq!(msg, "RESETCONF ORPort=443 ORListenAddress\r\n");
    }
    
    #[test]
    fn getconf_monadic() {
        let msg = DomainSocket::getconf(vec!["ORPort"]);
        assert_eq!(msg, "GETCONF ORPort\r\n");
    }
    
    #[test]
    fn getconf_variadic() {
        let msg = DomainSocket::getconf(vec!["ORPort", "ORListenAddress"]);
        assert_eq!(msg, "GETCONF ORPort ORListenAddress\r\n");
    }
    
    #[test]
    fn setevents_monadic() {
        let msg = DomainSocket::setevents(vec!["CIRC"], false);
        assert_eq!(msg, "SETEVENTS CIRC\r\n");
    }
    
    #[test]
    fn setevents_variadic() {
        let msg = DomainSocket::setevents(vec!["CIRC", "STREAM"],
                                           true);
        assert_eq!(msg, "SETEVENTS EXTENDED CIRC STREAM\r\n");
    }
    
    #[test]
    fn authenticate_monadic() {
        let msg = DomainSocket::authenticate("secret_password");
        assert_eq!(msg, "AUTHENTICATE secret_password\r\n");
    }
    
    #[test]
    fn saveconf_monadic() {
        let msg = DomainSocket::saveconf();
        assert_eq!(msg, "SAVECONF\r\n");
    }
    
    #[test]
    fn mapaddress_monadic() {
        let msg = DomainSocket::mapaddress(vec![("0.0.0.0", "torproject.org")]);
        assert_eq!(msg, "MAPADDRESS 0.0.0.0=torproject.org\r\n");
    }
    
    #[test]
    fn mapaddress_variadic() {
        let msg = DomainSocket::mapaddress(
            vec![("0.0.0.0", "torproject.org"),
                 ("1.2.3.4", "tor.freehaven.net")]);
        assert_eq!(
            msg,
            "MAPADDRESS 0.0.0.0=torproject.org 1.2.3.4=tor.freehaven.net\r\n");
    }
    
    #[test]
    fn signal_monadic() {
        let msg = DomainSocket::signal(Signal::TERM);
        assert_eq!(msg, "SIGNAL TERM\r\n");
    }
    
    #[test]
    fn getinfo_monadic() {
        let msg = DomainSocket::getinfo(vec!["version"]);
        assert_eq!(msg, "GETINFO version\r\n");
    }
    
    #[test]
    fn getinfo_variadic() {
        let msg = DomainSocket::getinfo(vec!["version", "config-text"]);
        assert_eq!(msg, "GETINFO version config-text\r\n");
    }
    
    #[test]
    fn extendcircuit_monadic() {
        let msg = DomainSocket::extendcircuit("TODO_REAL_ID",
                                               Some(vec!["TODO_REAL_SPEC"]),
                                               Some(Purpose::General));
        assert_eq!(
            msg,
            "EXTENDCIRCUIT TODO_REAL_ID TODO_REAL_SPEC purpose=general\r\n");
    }
    
    #[test]
    fn extendcircuit_variadic() {
        let msg = DomainSocket::extendcircuit("TODO_REAL_ID",
                                               Some(vec!["TODO_REAL_SPEC",
                                                         "REAL_SPEC2"]),
                                               Some(Purpose::Controller));
        assert_eq!(
            msg,
            "EXTENDCIRCUIT TODO_REAL_ID TODO_REAL_SPEC,REAL_SPEC2 purpose=controller\r\n");
    }
    
    #[test]
    fn setcircuitpurpose_monadic() {
        let msg = DomainSocket::setcircuitpurpose("TODO_REAL_ID",
                                                  Purpose::Controller);
        assert_eq!(msg, "SETCIRCUITPURPOSE TODO_REAL_ID purpose=controller\r\n");
    }
    
    #[test]
    fn attachstream_monadic() {
        let msg = DomainSocket::attachstream("TODO_STREAM_ID",
                                              "0",
                                              None);
        assert_eq!(msg, "ATTACHSTREAM TODO_STREAM_ID 0\r\n");
    }
    
    #[test]
    fn attachstream_variadic() {
        let msg = DomainSocket::attachstream("TODO_STREAM_ID",
                                              "TODO_CIRCUIT_ID",
                                              Some(5));
        assert_eq!(msg, "ATTACHSTREAM TODO_STREAM_ID TODO_CIRCUIT_ID HOP=5\r\n");
    }
    
    #[test]
    fn postdescriptor_monadic() {
        let msg = DomainSocket::postdescriptor("TODO_DESCRIPTOR",
                                                Some(Purpose::Bridge),
                                                Some(true));
        assert_eq!(msg, "+POSTDESCRIPTOR purpose=bridge cache=yes\r\nTODO_DESCRIPTOR\r\n.\r\n");
    }
    
    #[test]
    fn postdescriptor_variadic() {
        let msg = DomainSocket::postdescriptor("TODO_DESCRIPTOR",
                                                None, None);
        assert_eq!(msg, "+POSTDESCRIPTOR\r\nTODO_DESCRIPTOR\r\n.\r\n");
    }
    
    #[test]
    fn redirectstream_monadic() {
        let msg = DomainSocket::redirectstream("TODO_STREAM_ID",
                                                "TODO_ADDRESS",
                                                None);
        assert_eq!(msg, "REDIRECTSTREAM TODO_STREAM_ID TODO_ADDRESS\r\n");
    }
    
    #[test]
    fn redirectstream_variadic() {
        let msg = DomainSocket::redirectstream("TODO_STREAM_ID",
                                                "TODO_ADDRESS",
                                                Some(57600));
        assert_eq!(msg, "REDIRECTSTREAM TODO_STREAM_ID TODO_ADDRESS 57600\r\n");
    }
    
    #[test]
    fn closestream_monadic() {
        let msg = DomainSocket::closestream("TODO_STREAM_ID",
                                             StreamCloseReason::MISC,
                                             None);
        assert_eq!(msg, "CLOSESTREAM TODO_STREAM_ID 1\r\n");
    }
    
    #[test]
    fn closestream_variadic() {
        let msg = DomainSocket::closestream("TODO_STREAM_ID",
                                             StreamCloseReason::DONE,
                                             Some(vec!["FALSE_FLAG"]));
        assert_eq!(msg, "CLOSESTREAM TODO_STREAM_ID 6 FALSE_FLAG\r\n");
    }
    
    #[test]
    fn closecircuit_monadic() {
        let msg = DomainSocket::closecircuit("TODO_CIRCUIT_ID", None);
        assert_eq!(msg, "CLOSECIRCUIT TODO_CIRCUIT_ID\r\n");
    }
    
    #[test]
    fn closecircuit_variadic() {
        let msg = DomainSocket::closecircuit("TODO_CIRCUIT_ID",
                                              Some(vec!["IfUnused"]));
        assert_eq!(msg, "CLOSECIRCUIT TODO_CIRCUIT_ID IfUnused\r\n");
    }
    
    #[test]
    fn quit_monadic() {
        let msg = DomainSocket::quit();
        assert_eq!(msg, "QUIT\r\n");
    }
    
    #[test]
    fn usefeature_monadic() {
        let msg = DomainSocket::usefeature(vec!["EXTENDED_EVENTS"]);
        assert_eq!(msg, "USEFEATURE EXTENDED_EVENTS\r\n");
    }
    
    #[test]
    fn usefeature_variadic() {
        let msg = DomainSocket::usefeature(vec!["EXTENDED_EVENTS", "VERBOSE_NAMES"]);
        assert_eq!(msg, "USEFEATURE EXTENDED_EVENTS VERBOSE_NAMES\r\n");
    }
    
    #[test]
    fn resolve_monadic() {
        let msg = DomainSocket::resolve(Some(vec!["mode=reverse"]),
                                         Some(vec!["TODO_ADDRESS"]));
        assert_eq!(msg, "RESOLVE mode=reverse TODO_ADDRESS\r\n");
    }
    
    #[test]
    fn resolve_variadic() {
        let msg = DomainSocket::resolve(None, Some(vec!["TODO_ADDRESS"]));
        assert_eq!(msg, "RESOLVE TODO_ADDRESS\r\n");
    }
    
    #[test]
    fn protocolinfo_monadic() {
        let msg = DomainSocket::protocolinfo();
        assert_eq!(msg, "PROTOCOLINFO 1\r\n");
    }
    
    #[test]
    fn loadconf_monadic() {
        let msg = DomainSocket::loadconf("TODO_TOR_CONF");
        assert_eq!(msg, "+LOADCONF\r\nTODO_TOR_CONF\r\n.\r\n");
    }
    
    #[test]
    fn takeownership_monadic() {
        let msg = DomainSocket::takeownership();
        assert_eq!(msg, "TAKEOWNERSHIP\r\n");
    }
    
    #[test]
    fn authchallenge_monadic() {
        let msg = DomainSocket::authchallenge("TODO_SAFECOOKIE",
                                               "TODO_CLIENT_NONCE");
        assert_eq!(msg, "AUTHCHALLENGE TODO_SAFECOOKIE TODO_CLIENT_NONCE\r\n");
    }
    
    #[test]
    fn dropguards_monadic() {
        let msg = DomainSocket::dropguards();
        assert_eq!(msg, "DROPGUARDS\r\n");
    }
    
    #[test]
    fn hsfetch_monadic() {
        let msg = DomainSocket::hsfetch("TODO_ADDRESS", None);
        assert_eq!(msg, "HSFETCH TODO_ADDRESS\r\n");
    }
    
    #[test]
    fn hsfetch_variadic() {
        let msg = DomainSocket::hsfetch("TODO_ADDRESS",
                                         Some("TODO_SERVER_NAME"));
        assert_eq!(msg, "HSFETCH TODO_ADDRESS SERVER=TODO_SERVER_NAME\r\n");
    }
    
    #[test]
    fn add_onion_monadic() {
        let msg = DomainSocket::add_onion(OnionKeyType::NEW,
                                           OnionKeyBlob::BEST,
                                           None,
                                           None,
                                           None);
        assert_eq!(msg, "ADD_ONION NEW:BEST\r\n");
    }
    
    #[test]
    fn add_onion_variadic() {
        let msg = DomainSocket::add_onion(OnionKeyType::RSA1024,
                                           OnionKeyBlob::String("TODO_RSA_BLOB"),
                                           Some(vec!["DiscardPK", "Detach"]),
                                           Some((80, None)),
                                           Some(vec![("alice", None)]));
        assert_eq!(msg, "ADD_ONION RSA1024:TODO_RSA_BLOB Flags=DiscardPK,Detach Port=80 ClientAuth=alice\r\n");
    }
    
    #[test]
    fn del_onion_monadic() {
        let msg = DomainSocket::del_onion("TODO_SERVICEID");
        assert_eq!(msg, "DEL_ONION TODO_SERVICEID\r\n");
    }
    
    #[test]
    fn hspost_monadic() {
        let msg = DomainSocket::hspost("TODO_DESCRIPTOR", None);
        assert_eq!(msg, "+HSPOST\r\nTODO_DESCRIPTOR\r\n.\r\n");
    }
    
    #[test]
    fn hspost_variadic() {
        let msg = DomainSocket::hspost("TODO_DESCRIPTOR", Some("TODO_SERVERID"));
        assert_eq!(msg, "+HSPOST SERVER=TODO_SERVERID\r\nTODO_DESCRIPTOR\r\n.\r\n");
    }
}
