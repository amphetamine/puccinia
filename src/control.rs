extern crate chrono;
use std::fs::OpenOptions;
use std::io::{BufReader, BufWriter, BufRead, Write,
              Result as ioResult,
              Error as ioError};
use std::net::{TcpStream, SocketAddr, IpAddr, Shutdown};
use std::os::unix::net::UnixStream;
use std::path::Path;
use chrono::{UTC, DateTime, Duration};

static SOCKET_BUFFER_SIZE: usize = 8192;

// TODO consistent error handling--don't just return err.to_string() or w/e

/// Basic socket functions for the controller.
pub trait ControlSocket<'a> {
    /// Sends a message over the socket.
    fn send(&'a self, message: &str) -> Result<(), ioError>;
    /// Reads a message from the socket, returning it as a vector of lines.
    fn recv(&'a self) -> Result<Vec<String>, ioError>;
    /// Gets the socket's address.
    fn get_address(&'a self) -> String;
    /// Gets the socket's port.
    fn get_port(&'a self) -> u16;
    // fn is_alive(&'a self) -> bool;
    /// Indicates whether or not the socket is connected locally.
    fn is_localhost(&'a self) -> bool;
    /// Reports how long the connection has been active.
    fn connection_time(&'a self) -> Duration;
    /// Closes the socket.
    fn close(self) -> Result<(), ioError>;
}

/// Representation of a connection through a UNIX domain socket.
pub struct DomainSocket {
    socket_path: String,
    socket: UnixStream,
    time_connected: DateTime<UTC>
}

/// Representation of a connection through TCP.
pub struct ControlPort {
    address: SocketAddr,
    stream: TcpStream,
    time_connected: DateTime<UTC>
}

fn open_socket(dest_path: &str) -> ioResult<DomainSocket> {
    let socket = try!(UnixStream::connect(dest_path));
    Ok(DomainSocket {
        socket_path: dest_path.to_string(),
        socket: socket,
        time_connected: UTC::now()
    })    
}

impl DomainSocket {
    /// Attempts to create a new domain socket connection. 
    pub fn new(dest_path: &str) -> ioResult<DomainSocket> {
        if !Path::new(dest_path).exists() {
            OpenOptions::new().create(true).write(true).open(dest_path)
                .and_then(|_| open_socket(dest_path))
        } else { open_socket(dest_path) }
    }
}

impl<'a> ControlSocket<'a> for DomainSocket {
    fn get_address(&self) -> String {
        self.socket_path.to_string()
    }
    fn get_port(&self) -> u16 {
        // TODO express better that sockets have no port or otherwise refactor
        return 0;
    }
    fn send(&'a self, message: &str) -> Result<(), ioError> {
        let mut writer = BufWriter::new(&self.socket);
        writer.write_all(message.as_bytes())
    }
    fn recv(&'a self) -> Result<Vec<String>, ioError> {
        let reader = BufReader::new(&self.socket);        
        let mut buf: Vec<String> = Vec::with_capacity(2048);
        for line in reader.lines() {
            match line {
                Ok(info_line) => {
                    // TODO detect actual status code serving as EOM
                    let was_eom = info_line == "250 OK" ||
                        info_line.contains("250 AUTHCHALLENGE");
                    buf.push(info_line);
                    if was_eom { break; }
                },
                Err(line_error) => {
                    // Early return with error if a single line fails.
                    return Err(line_error);
                }
            }
        }
        Ok(buf)
    }
    // fn is_alive(&'a self) -> bool;
    fn is_localhost(&self) -> bool {
        // TODO if there is some crazy remote socket stuff going on, check for
        // it
        true
    }
    fn connection_time(&'a self) -> Duration {
        chrono::UTC::now() - self.time_connected
    }
    // fn connect(&self); // TODO need this kind of stateful info? new == connect?
    fn close(self) -> Result<(), ioError> {
        self.socket.shutdown(Shutdown::Both)
    }
}

impl ControlPort {
    /// Attempts to create a new TCP connection.
    pub fn new(dest_addr: &str, dest_port: u16) -> ioResult<ControlPort> {
        let stream = try!(TcpStream::connect((dest_addr, dest_port)));
        // If the connection succeeds, below should be fine to unwrap
        let address = stream.peer_addr().unwrap();
        
        // return constructed instance
        Ok(ControlPort {
            address: address,
            stream: stream,
            time_connected: UTC::now()
        })
    }
}

impl<'a> ControlSocket<'a> for ControlPort {
    fn get_address(&self) -> String {
        self.address.to_string()
    }
    fn get_port(&self) -> u16 {
        self.address.port()
    }
    fn send(&'a self, message: &str) -> Result<(), ioError> {
        let mut writer = BufWriter::new(&self.stream);
        writer.write_all(message.as_bytes())
    }
    fn recv(&self) -> Result<Vec<String>, ioError> {
        let reader = BufReader::new(&self.stream);        
        let mut buf: Vec<String> = Vec::with_capacity(SOCKET_BUFFER_SIZE);
        for line in reader.lines() {
            match line {
                Ok(info_line) => {
                    // TODO detect actual status code serving as EOM
                    let was_eom = info_line == "250 OK" ||
                        info_line.contains("250 AUTHCHALLENGE");
                    buf.push(info_line);
                    if was_eom { break; }
                },
                Err(line_error) => {
                    // Early return with error if a single line fails.
                    return Err(line_error);
                }
            }
        }
        Ok(buf)
    }
    // fn is_alive(&mut self) -> bool {
    //     // TODO is this even necessary?
    // }
    fn is_localhost(&self) -> bool {
        // this just wraps the currently-unstable is_loopback() method for now
        match self.address.ip() {
            // enumerate over IpAddr enum since is_loopback() is not there yet
            IpAddr::V4(address) => address.is_loopback(),
            IpAddr::V6(address) => address.is_loopback()
        }
    }
    fn connection_time(&self) -> Duration {
        chrono::UTC::now() - self.time_connected
    }
    fn close(self) -> Result<(), ioError> {
        self.stream.shutdown(Shutdown::Both)
    }
}


#[cfg(test)]
mod tests {
    use super::{ControlPort, ControlSocket, DomainSocket};

    #[test]
    fn controlport_constructor() {
        let test_ip = "127.0.0.1";
        let test_port = 9051;
        let ip_string = test_ip.to_string() + ":" + &(test_port).to_string();
        
        let test_control_port = ControlPort::new(test_ip, test_port).unwrap();
        assert_eq!(test_control_port.get_address(), ip_string);
        assert_eq!(test_control_port.get_port(), test_port);
        assert!(test_control_port.is_localhost());
    }

    #[test]
    #[should_panic]
    fn controlport_constructor_bad_addr() {
        let test_ip = "9999999";
        let test_port = 9051;
        ControlPort::new(test_ip, test_port).unwrap();
    }

    #[test]
    fn controlport_destructor() {
        let test_ip = "127.0.0.1";
        let test_port = 9051;
        let ip_string = test_ip.to_string() + ":" + &(test_port).to_string();

        let test_control_port = ControlPort::new(test_ip, test_port).unwrap();
        test_control_port.close().ok().expect("Should have closed read socket properly.");
    }

    #[test]
    fn domainsocket_constructor() {
        let test_sockfile = "./torsocket";
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();
        assert_eq!(test_domain_socket.get_address(), test_sockfile);
        assert_eq!(test_domain_socket.get_port(), 0);
        assert!(test_domain_socket.is_localhost());
    }

    // #[should_panic(expected = "Could not connect with given addresses:")]
    #[test]
    #[should_panic]
    fn domainsocket_constructor_bad_path() {
        let test_path = "/foo/bar";
        DomainSocket::new(test_path).unwrap();
    }

    #[test]
    fn domainsocket_destructor() {
        let test_sockfile = "./torsocket";
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();
        test_domain_socket.close().ok().expect("Should have closed socket properly.");
    }
}
