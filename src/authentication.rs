use std::io::{Error as ioError};
use hex::{FromHex, ToHex};
use hmac::{Hmac, Mac};
use rand::{OsRng, Rng};
use sha2::Sha256;
use control::{DomainSocket, ControlSocket};
use reply::{Reply, AuthChallenge, parse_reply};

/// Functions for authenticating with a Tor process.
pub trait Authentication<'a> {
    /// Queries the Tor process for protocol info
    fn get_protocol_info(&self);
    /// Attempts to authenticate using whatever is serialized into `pass`.
    ///
    /// **Note:** Password authentication requires that the string be wrapped in
    /// double quotes (i.e. `\"password\"` as your input string).
    fn authenticate(&self, pass: &str) -> Result<Reply, ioError>; // TODO real error
    // Attempts to authenticate using challenge-and-response (SAFECOOKIE).
    /// Issues an auth challenge (for SAFECOOKIE).
    ///
    /// Returns both the server's reply and the generated nonce.
    fn auth_challenge(&self) -> Result<(Reply, String), ioError>; // TODO real error
    /// Issues an authenticate command with cookie and server AUTHCHALLENGE (for
    /// SAFECOOKIE).
    fn safe_cookie_auth(&self,
                        cookie: &str,
                        client_nonce: &str,
                        server_challenge: AuthChallenge) ->
        Result<Reply, ioError>;
}

impl<'a> Authentication<'a> for DomainSocket {
    fn get_protocol_info(&self) {
        // write "PROTOCOLINFO 1" to the buffer

        // NOTE from Stem code:
        //
        // Tor hangs up on sockets after receiving a PROTOCOLINFO query if it
        // isn't next followed by authentication. Transparently reconnect if
        // that happens.
        self.send("PROTOCOLINFO 1\r\n").unwrap();
        let msg = self.recv().unwrap();
        for line in msg { // TODO proper handling of result
            println!("{}", line);
        }
    }
    
    fn authenticate(&self, pass: &str) -> Result<Reply, ioError> {
        self.send(&format!("AUTHENTICATE {}\r\n", pass)).and_then(|_| {
            self.recv().and_then(|auth_reply| {
                Ok(parse_reply(&auth_reply))
            })                
        })
    }

    fn auth_challenge(&self) -> Result<(Reply, String), ioError> {
        // TODO review nonce generation for security
        let mut rng = OsRng::new().unwrap();
        let mut nonce = vec![0 as u8];
        rng.fill_bytes(nonce.as_mut_slice());
        let nonce_str = 
            format!("{:x}", nonce[0]);
        
        let challenge_str = "AUTHCHALLENGE SAFECOOKIE ".to_string() +
            &nonce_str +
            "\r\n";

        self.send(challenge_str.as_str()).and_then(|_| {
            self.recv().and_then(|auth_reply| {
                Ok((parse_reply(&auth_reply), nonce_str))
            })
        })
    }

    fn safe_cookie_auth(&self,
                        cookie: &str,
                        client_nonce: &str,
                        server_challenge: AuthChallenge)
                        -> Result<Reply, ioError> {
        let server_nonce = server_challenge.server_nonce.as_str();
        
        // verify server hash is good
        // HMAC-SHA256("Tor safe cookie authentication server-to-controller
        // hash", CookieString | ClientNonce | ServerNonce)
        let mut server_hmac =
            Hmac::<Sha256>::new(
                "Tor safe cookie authentication server-to-controller hash"
                    .as_bytes());

        let hmac_input: Vec<u8> = FromHex::from_hex(
            cookie.to_string() + client_nonce + server_nonce
        ).unwrap();
        
        server_hmac.input(hmac_input.as_slice());
                 
        let our_server_hash = server_hmac.result();
        let our_server_bytes = our_server_hash.code();
        let our_server_hex = our_server_bytes.to_hex();
        // TODO replace with error/check
        println!("our server hash: {}", &our_server_hex);
        println!("server hash: {}", &server_challenge.server_hash);
        
        // HMAC-SHA256("Tor safe cookie authentication controller-to-server
        // hash", CookieString | ClientNonce | ServerNonce)
        let mut client_hmac =
            Hmac::<Sha256>::new(
                "Tor safe cookie authentication controller-to-server hash"
                    .as_bytes());
        client_hmac.input(hmac_input.as_slice());
        
        // TODO review timing attack possibility cited by crate author about
        // this method
        let client_hash = client_hmac.result();
        let client_hashed_bytes = client_hash.code();
        let client_hash_hex = client_hashed_bytes.to_hex();

        self.authenticate(client_hash_hex.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::{Authentication};
    use control::{DomainSocket, ControlSocket};
    use reply::{Reply};
    use std::io::{Error, ErrorKind};
    use std::fs::File;
    use std::io::Read;
    use hex::ToHex;

    #[ignore]
    #[test]
    fn domainsocket_get_protocol_info() {
        let test_sockfile = "./torsocket";
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        test_domain_socket.get_protocol_info();
        assert!(false);
    }
    
    #[test]
    #[ignore]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn null_auth() {
        let test_sockfile = "./torsocket";
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        // In the null auth case, it doesn't actually matter what you send
        let res = test_domain_socket.authenticate("").unwrap();
        assert_eq!(res, Reply::Unknown(vec!["250 OK".to_string()]));
    }
    
    #[test]
    #[ignore]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn password_auth() {
        let test_sockfile = "./torsocket";
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        let res = test_domain_socket.authenticate("\"wub\"").unwrap();
        assert_eq!(res, Reply::Unknown(vec!["250 OK".to_string()]));
    }
    
    #[test]
    #[ignore]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn password_auth_failure() {
        let test_sockfile = "./torsocket";
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        // no one will ever set their password to this...right?
        let res = test_domain_socket.authenticate("\"password\"").unwrap();
        assert_eq!(res, Reply::AuthError("Authentication failed: Password did not match HashedControlPassword value from configuration".to_string()));
    }
    
    #[test]
    #[ignore]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn cookie_auth() {
        let test_sockfile = "./torsocket";
        
        let test_cookie_path = "./control_auth_cookie";                               
        let mut file = File::open(test_cookie_path).unwrap();
        let mut contents = Vec::new();
        file.read_to_end(&mut contents).unwrap();
        
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        let res = test_domain_socket
            .authenticate(contents.to_hex().as_str())
            .unwrap();
        assert_eq!(res, Reply::Unknown(vec!["250 OK".to_string()]));
    }
    
    #[test]
    #[ignore]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn cookie_auth_failure() {
        let test_sockfile = "./torsocket";
        let test_cookie_path = "./bad_auth_cookie";
        let mut file = File::open(test_cookie_path).unwrap();
        let mut contents = Vec::new();
        file.read_to_end(&mut contents).unwrap();
        
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        let res = test_domain_socket
            .authenticate(contents.to_hex().as_str())
            .unwrap();
        assert_eq!(res, Reply::AuthError("Authentication failed: Authentication cookie did not match expected value.".to_string()));
    }
    
    #[ignore]
    #[test]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn safecookie_auth() {
        let test_sockfile = "./torsocket";
        let test_cookie_path = "./control_auth_cookie";
        let mut file = File::open(test_cookie_path).unwrap();
        let mut contents = Vec::new();
        file.read_to_end(&mut contents).unwrap();
        
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        let res = test_domain_socket
            .auth_challenge()
            .and_then(|(reply, client_nonce)| {                
                let cookie_hex = contents.to_hex();
                if let Reply::AuthChallenge(auth) = reply {
                    test_domain_socket.safe_cookie_auth(cookie_hex.as_str(),
                                                        client_nonce.as_str(),
                                                        auth)
                } else {
                    Err(Error::new(ErrorKind::Other,
                                   format!("Did not get an authchallenge from server. Instead got {:?}", reply)))
                }
            }).or_else(|err| {
                println!("err: {:?}", &err);
                Err(err)
            }).and_then(|res| {
                println!("res: {:?}", &res);
                Ok(res)
            }).unwrap();

        assert_eq!(res, Reply::Unknown(vec!["250 OK".to_string()]));
    }
    
    #[ignore]
    #[test]
    // This test is ignored by default, since it relies on a specific setting in
    // the tor config.
    fn safecookie_auth_failure() {
        let test_sockfile = "./torsocket";
        let test_cookie_path = "./bad_auth_cookie";

        let mut file = File::open(test_cookie_path).unwrap();
        let mut contents = Vec::new();
        file.read_to_end(&mut contents).unwrap();
        
        let test_domain_socket = DomainSocket::new(test_sockfile).unwrap();

        let res = test_domain_socket
            .auth_challenge()
            .and_then(|(reply, client_nonce)| {                
                let cookie_hex = contents.to_hex();
                if let Reply::AuthChallenge(auth) = reply {
                    test_domain_socket.safe_cookie_auth(cookie_hex.as_str(),
                                                        client_nonce.as_str(),
                                                        auth)
                } else {
                    Err(Error::new(ErrorKind::Other,
                                   format!("Did not get an authchallenge from server. Instead got {:?}", reply)))
                }
            }).or_else(|err| {
                println!("err: {:?}", &err);
                Err(err)
            }).and_then(|res| {
                println!("res: {:?}", &res);
                Ok(res)
            }).unwrap();

        assert_eq!(res, Reply::AuthError("Authentication failed: Safe cookie response did not match expected value.".to_string()));
    }
}
