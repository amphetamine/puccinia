# CONTRIBUTING

## Preamble

Hey yall so this is primarily a repo for me to learn some things about how Tor
works, and also to write some good Rust that maybe helps people.

Like it says in the description, this is a fork of an unmaintained project, but
anyway it's GPLv3 so that should be no big deal.

## Goals

At this early phase the main thing I am aiming for is feature completeness and
basic correctness re: Tor spec. So, until that's done I want to prioritize
readable code and tests over everything else.

### Things I would *love* help with

- Tickets planning out features to build: let's talk!
- Merge requests around tickets
- Code review so it's not just me checking in weird stuff

## Requirements

- All added code should be backed by tests and docs
- Try to stick to Rust standard formatting unless there's a conversation about
  it

![be excellent to each other](./be_excellent_to_each_other.gif)
